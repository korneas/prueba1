var view = {
    getContenedor: function (show, season) {
        var contenedor = document.createElement('div');
        contenedor.innerHTML = `
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img class="img-responsive" src="${show.image}">
                </div>
                <div class="col-md-8">
                    <h1>${show.name}</h1>
                    <a>${show.url}</a>
                    <div id="dinamico">
                    </div>
                </div>
            </div>
        </div>
        `;

        var dinamico = contenedor.querySelector('#dinamico');
        dinamico.appendChild(this.getTexto(show));
        if (season != null) {
            dinamico.innerHTML = '';
            dinamico.appendChild(this.getEpisodes(season));
        }
        return contenedor;
    },

    getSeasonClick: function (text) {
        var that = this;
        text.querySelectorAll('button').forEach(function (btn, index) {
            btn.addEventListener('click', function () {
                that.onSeasonClick(index);
            });
        });
    },

    getSummaryClick: function getSummaryClick(content) {
        var that = this;
        content.querySelectorAll('button')[0].addEventListener('click', function () {
            that.onSummaryClick();
        });
    },

    getWatchedClick: function getWatchedClick(content,season,seasonNum) {
        var that = this;
        content.querySelectorAll('.watched').forEach(function (btn, index) {
            btn.addEventListener('click', function () {
                that.onWatchClick(index,season,seasonNum);
            })
        });
    },

    getTexto: function (show) {
        var texto = document.createElement('div');
        texto.innerHTML = '<br>' + show.summary + '<br>';
        texto.innerHTML += '<b>Generos: </b>' + show.genres + '<br><br>';
        texto.innerHTML += '<b>Seasons: </b>' + '<br>' + buttons(show);

        function buttons(show) {
            var text = '';
            for (i = 0; i < show.seasonCount; i++) {
                text += '<button type="button" class="btn btn-primary" style="margin: 10px;">Season ' + (i + 1) + '</button>';
            }
            return text;
        }

        this.getSeasonClick(texto);
        return texto;
    },

    //Obtener episodios
    getEpisodes: function getEpisodes(season) {
        var content = document.createElement('div');

        content.innerHTML = `
        <br>
        <h1 style="line-height: 50%;">Season 1</h1>
        <h4 style="color: grey;">${season.length} episodes</h4>
        <button type="button" class="btn btn-primary" style="right: 0; position: absolute;">Summary</button>
        <br>`;

        for (var i = 0; i < season.length; i++) {
            content.innerHTML += `
            <div style="display: inline-block; margin: 10px;">
                <img src="${season[i].image}">
                <p><b>${season[i].name}</b> <br> ${season[i].date}</p>
                <button type="button" class="btn btn-primary"><a href="${season[i].url}" style="color: white;">Link</a></button>
                <button type="button" class="btn btn-danger watched">No visto</button>
            </div>
            `;
        }

        this.getSummaryClick(content);
        return content;
    },

    //Metodo para renderizar
    render: function (show, season) {
        var main = document.getElementById('container');
        var contenedor = this.getContenedor(show, season);

        main.innerHTML = '';
        main.appendChild(contenedor);
    }
};
