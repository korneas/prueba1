var createModel = function createModel(initialData){
  var info = {
    name: '',
    summary: '',
    image: '',
    genres: [''],
    url: '',
    seasonCount: 0,

    // hidden
    seasons: [ // list of seasons
      [ // list of episodes in season
        {
          name: '',
          date: '',
          image: '',
          url: '',
        }
      ]
    ]
  };

  info = transformData(initialData);

  return {
    getInfo: function getInfo(){
      var infoRet = Object.assign({}, info);
      delete infoRet.seasons;
      return infoRet;
    },
  
    getSeason: function getSeason(index){
      return info.seasons[index];
    },

    setWatched: function setWatched(season, episode, watched){
      info.seasons[season][episode].watched = watched;
    }
  }
}



function transformData(data){
  var info = {
    name: data.name,
    summary: data.summary,
    image: data.image.original,
    genres: data.genres,
    url: data.url,
    seasons: data._embedded.episodes.reduce(function(seasons, currEp){
      var episode = {
        name: currEp.name,
        date: currEp.airdate,
        image: currEp.image.medium,
        url: currEp.url,
        watched: false,
      };

      if(!seasons[currEp.season - 1]) seasons[currEp.season - 1] = [];
      seasons[currEp.season - 1][currEp.number - 1] = episode;

      return seasons;
    }, [])
  };
  info.seasonCount = info.seasons.length;
  return info;
}