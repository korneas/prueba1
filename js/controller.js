var controller = function controller(model, view) {
    var info = model.getInfo();

    view.render(info);

    view.onSeasonClick = function onSeasonClick(index) {
        view.render(info, model.getSeason(index));
    };

    view.onSummaryClick = function onSummaryClick() {
        view.render(info);
    };
    
    view.onWatchClick = function onWatchClick(index,season,seasonNum) {
        model.getSeason(seasonNum)[index].watched = !model.getSeason(seasonNum)[index].watched;
        view.render(info, model.getSeason(seasonNum));
    };

};


var model = createModel(data);
controller(model, view);
